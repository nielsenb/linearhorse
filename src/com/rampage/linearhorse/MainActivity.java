package com.rampage.linearhorse;

import java.util.ArrayList;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity implements OnClickListener, SensorEventListener {
	//TODO: Generalize for non-92 Corvette use-cases
	
	private static int MILLISECONDS_PER_SECOND = 1000;
	private static float METERS_PER_SECOND_PER_MPH = 0.44704f;
	
	private static float TARGET_MPH = 60;
	private static float TARGET_MPH_IN_METERS_PER_SECOND = TARGET_MPH * METERS_PER_SECOND_PER_MPH;
	
	private static float RUN_START_METERS_PER_SECOND = (float) (0.1 * SensorManager.GRAVITY_EARTH); // Speed required to start a run

	private static int VEHICLE_WEIGHT_IN_LBS = 3623;
	
	private static int SAMPLES_FOR_CALIBRATION = 50;
	
	SensorManager sensorManager;
	Sensor linearAccelerometer;
	
	TextView xRawTextView;
	TextView yRawTextView;
	TextView zRawTextView;
	TextView xCorrectedTextView;
	TextView yCorrectedTextView;
	TextView zCorrectedTextView;
	TextView statusTextView;
	
	Button calibrateButton;
	Button armButton;
	
	boolean calibrating = false;
	boolean isCalibrated = false;
	int calibrationSamples = 0;
	
	boolean armed = false;
	boolean recording = false;
	
	//Declare a place to store our samples during a run
	ArrayList<Float> samples = new ArrayList<Float>(); //Array lists of primitives not allowed...?
	float speedAccumulator = 0;
	long runStartTime; //The run start time in milliseconds, we keep track of this since we have no way of fixing the interval between samples
	long runElapsedTime; //Records the elapsed run time in milliseconds
	long thisSampleTime = 0; //The time of the current sample
	long lastSampleTime = 0; //The time of the last sample
	
	float xCalibrationAccumulator = 0;
	float yCalibrationAccumulator = 0;
	float zCalibrationAccumulator = 0;
	
	float xCorrectionValue = 0;
	float yCorrectionValue = 0;
	float zCorrectionValue = 0;
	
	float xOrientation;
	float yOrientation;
	float zOrientation;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //Bind our views
        xRawTextView = (TextView) findViewById(R.id.xraw_text);
        yRawTextView = (TextView) findViewById(R.id.yraw_text);
        zRawTextView = (TextView) findViewById(R.id.zraw_text);
        xCorrectedTextView = (TextView) findViewById(R.id.xcorrected_text);
        yCorrectedTextView = (TextView) findViewById(R.id.ycorrected_text);
        zCorrectedTextView = (TextView) findViewById(R.id.zcorrected_text);
        statusTextView = (TextView) findViewById(R.id.status_text);
        
        calibrateButton = (Button) findViewById(R.id.calibrate_btn);
        calibrateButton.setOnClickListener(this);
        armButton = (Button) findViewById(R.id.arm_btn);
        armButton.setOnClickListener(this);
        armButton.setEnabled(false); //Only enabled after calibration is complete
        
        //Hide the corrected views
        xCorrectedTextView.setVisibility(View.INVISIBLE);
        yCorrectedTextView.setVisibility(View.INVISIBLE);
        zCorrectedTextView.setVisibility(View.INVISIBLE);
        
        //Don't let this view sleep
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        //Lock orientation since orientation changes reset listeners on some devices...
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        
        //Request a sensor manager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        
        //Grab a linear accelerometer, this will be gravity corrected, and gyroscope adjusted to phone X, Y, and Z
        //TODO: Handle case where linear acceleration sensor is not available
        //linearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        linearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        
        //Register our app as a listener
        //Note that the sample time is only 'suggested', we still need to measure the actual elapsed time,
        //also note that setting a custom interval does not work, and has apparently never worked
        sensorManager.registerListener(this, linearAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

	public void onClick(View v) {
		switch (v.getId())
		{
		case R.id.calibrate_btn:
			xCalibrationAccumulator = 0;
			yCalibrationAccumulator = 0;
			zCalibrationAccumulator = 0;
			
			isCalibrated = false;
			calibrating = true;
			
			calibrateButton.setEnabled(false);
			armButton.setEnabled(false);
	        xCorrectedTextView.setVisibility(View.INVISIBLE);
	        yCorrectedTextView.setVisibility(View.INVISIBLE);
	        zCorrectedTextView.setVisibility(View.INVISIBLE);
			
			break;
		case R.id.arm_btn:
			calibrateButton.setEnabled(false);
			armButton.setEnabled(false);
			
			statusTextView.setText(getString(R.string.armed));
			
			samples.clear(); //Restart with a fresh set of samples
			speedAccumulator = 0;
			thisSampleTime = 0;
			lastSampleTime = 0;
			armed = true;
			
			break;
		}
	}
    
    protected void onResume() {
    	//When the app gets focus back, we need to reregister the listener
    	super.onResume();
    	sensorManager.registerListener(this, linearAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }
    
    protected void onPause() {
    	//When the app goes to the background, unregister the listener
    	super.onPause();
    	sensorManager.unregisterListener(this);
    }
    
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public void onSensorChanged(SensorEvent event) {
		//Extract the acceleration values from the event
		float xAccel = event.values[SensorManager.DATA_X];
		float yAccel = event.values[SensorManager.DATA_Y];
		float zAccel = event.values[SensorManager.DATA_Z];
		
		//We take 100 samples to calibrate
		if (calibrating == true)
		{
			xCalibrationAccumulator += xAccel;
			yCalibrationAccumulator += yAccel;
			zCalibrationAccumulator += zAccel;
			
			calibrationSamples += 1;
			
			if (calibrationSamples >= SAMPLES_FOR_CALIBRATION)
			{
				xCorrectionValue = xCalibrationAccumulator / (float) calibrationSamples;
				yCorrectionValue = yCalibrationAccumulator / (float) calibrationSamples;
				zCorrectionValue = zCalibrationAccumulator / (float) calibrationSamples;
				
				//Reset the UI so we can recalibrate if we so desire
				calibrating = false;
				isCalibrated = true;
				calibrationSamples = 0;
				calibrateButton.setEnabled(true);
				armButton.setEnabled(true);
				
				xCorrectedTextView.setVisibility(View.VISIBLE);
				yCorrectedTextView.setVisibility(View.VISIBLE);
				zCorrectedTextView.setVisibility(View.VISIBLE);
			}
		}
		
		//Update the raw displays
		xRawTextView.setText("X: " + String.format("%.2f", xAccel));
		yRawTextView.setText("Y: " + String.format("%.2f", yAccel));
		zRawTextView.setText("Z: " + String.format("%.2f", zAccel));
		
		//If the sensor is calibrated, updated the corrected displays
		if (isCalibrated == true)
		{
			xCorrectedTextView.setText("X: " + String.format("%.2f", applyCorrectionValue(xAccel, xCorrectionValue)));
			yCorrectedTextView.setText("Y: " + String.format("%.2f", applyCorrectionValue(yAccel, yCorrectionValue)));
			zCorrectedTextView.setText("Z: " + String.format("%.2f", applyCorrectionValue(zAccel, zCorrectionValue)));
		}
		
		if (armed == true)
		{
			//Check to see if we've surpassed the acceleration threshold we've set to start a pass		
			if (Math.abs(applyCorrectionValue(yAccel, yCorrectionValue)) >= RUN_START_METERS_PER_SECOND && recording == false)
			{
				//Adjust the correction factor to the direction we're accelerating in
				if (applyCorrectionValue(xAccel, xCorrectionValue) < 0)
				{
					xOrientation = -1;
				}
				else
				{
					xOrientation = 1;
				}
				
				if (applyCorrectionValue(yAccel, yCorrectionValue) <  0)
				{
					yOrientation = -1;
				}
				else
				{
					yOrientation = 1;
				}
					
				if (applyCorrectionValue(zAccel, zCorrectionValue) < 0)
				{
					zOrientation = -1;
				}
				{
					zOrientation = 1;
				}
				
				statusTextView.setText(getString(R.string.recording)); //Set display to "Recording..."
				recording = true;
				runStartTime = System.currentTimeMillis();
			}
			
			if (recording == true)
			{
				//Figure out the amount of time this sample took to produce, only add a sample if we can determine this interval
				if (lastSampleTime == 0)
				{
					//Just set them to something to keep this conditional from firing again
					lastSampleTime = System.currentTimeMillis();
					thisSampleTime = System.currentTimeMillis();
				}
				else
				{
					//Note that we naively assume that all acceleration is in the direction we're attempting to measure
					//horsepower in, this is a terrible assumption, but hopefully a good enough approximation
					//float totalAcceleration = xOrientation * (xAccel - xCorrectionValue) + yOrientation * (yAccel - yCorrectionValue) + zOrientation * (zAccel - zCorrectionValue);
					float totalAcceleration = yOrientation * applyCorrectionValue(yAccel, yCorrectionValue);
					
					lastSampleTime = thisSampleTime;
					thisSampleTime = System.currentTimeMillis();
					
					long elapsedSampleTime = thisSampleTime - lastSampleTime;
					
					//Record our sample
					samples.add(totalAcceleration);
					speedAccumulator += totalAcceleration * (elapsedSampleTime / (float) MILLISECONDS_PER_SECOND);
					runElapsedTime = System.currentTimeMillis() - runStartTime;
				
					//We've reach 60 MPH, stop recording and calculate avg. HP
					if (speedAccumulator >= TARGET_MPH_IN_METERS_PER_SECOND)
					{
						armed = false;
						recording = false;
					
						//Display the average horsepower
						statusTextView.setText(String.format("%.2f", calculateAvgHP(runElapsedTime)) + " " + getString(R.string.hpavg) + "\n" + String.format("%.2f", (float) runElapsedTime / (float) MILLISECONDS_PER_SECOND) + " sec.");
					
						//Renable the arm button in case we want to do another pass
						calibrateButton.setEnabled(true);
						armButton.setEnabled(true);
					}
					else if (runElapsedTime / MILLISECONDS_PER_SECOND >= 15)
					{
						//If more than 15 seconds have elapsed, abort the run
						statusTextView.setText(getString(R.string.timedout)); //Set display to "Timed out."
					    //statusTextView.setText(String.format("%.2f", speedAccumulator / METERS_PER_SECOND_PER_MPH));
						
						armed = false;
						recording = false;
						calibrateButton.setEnabled(true);
						armButton.setEnabled(true);
					}
				}
			}
		}
	}
	
	private float applyCorrectionValue(float accelerationValue, float correctionValue) {
		float cosTheta;
		
		//The correction value is equal to g * cos(theta)
		if (correctionValue >= SensorManager.GRAVITY_EARTH)
		{
			cosTheta = (0.98f * SensorManager.GRAVITY_EARTH) / SensorManager.GRAVITY_EARTH;
		}
		else
		{
			cosTheta = correctionValue / SensorManager.GRAVITY_EARTH;
		}
		
		float sinTheta = FloatMath.sqrt(1 - (float) Math.pow(cosTheta, 2));
		//return (accelerationValue - correctionValue) * sinTheta + (accelerationValue - correctionValue) * cosTheta;
		return accelerationValue - correctionValue;
	}
	
	private float calculateAvgHP(double elapsedZerotoSixty) {
		//Calculate avg. horsepower from elapsed 0 to 60 time (in ms)
		double elapsedZerotoSixtyinSec = elapsedZerotoSixty / MILLISECONDS_PER_SECOND;
		
		double powerTemp = Math.pow(elapsedZerotoSixtyinSec, 1.33);
		double avgHP = (0.935 * VEHICLE_WEIGHT_IN_LBS) / powerTemp;
		
		return (float) avgHP; //We throw away some precision so its easier to format the result
	}
}
